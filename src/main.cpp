/*
 * main.cpp
 *
 */
#include <stdio.h>
#include <string.h>

#include <CHeaders/XPLM/XPLMPlugin.h>
#include <CHeaders/XPLM/XPLMPlanes.h>
#include <CHeaders/XPLM/XPLMDataAccess.h>
#include <CHeaders/XPLM/XPLMProcessing.h>
#include <CHeaders/XPLM/XPLMUtilities.h>

PLUGIN_API int XPluginStart(
    char *		outName,
    char *		outSig,
    char *		outDesc)
{
    // Define the plugin properties
    strcpy(outName, "DigiSky - VTOL");
    strcpy(outSig, "it.digisky.vtol");
    strcpy(outDesc, "VTOL");
    XPLMEnableFeature("XPLM_USE_NATIVE_PATHS", 1);

    return 1;
}

PLUGIN_API void	XPluginStop(void)
{
}

PLUGIN_API int XPluginEnable(void)
{    
    return 1;
}

PLUGIN_API void XPluginDisable(void)
{
}

PLUGIN_API void XPluginReceiveMessage(
    XPLMPluginID inFrom,
    long inMessage,
    void *param)
{
}